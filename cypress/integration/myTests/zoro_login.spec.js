/// <reference types= "Cypress"/>

describe ('Login test',  ()=> {

    it('Zoro - successfull login', ()=> {
        // the user visit "https://www.zoro.co.uk/" and lands on the home page
        // the user verify "Title" 
        // the user verify if the header contains "Log in" button
        // the user click on "Log in" button and "Sign in to your account" window pop-up
        // the user enters VALID credentials
        // the user click on "Sign in button" and logged in to their account
        // the user verify "Title" again after login
        // the again the user verify the Page Header contains "My Account"
        
        cy.visit('https://www.zoro.co.uk/')
        cy.title().should('include','Zoro UK - Experts in Hand Tools, Power Tools and PPE')
        cy.contains('Log in')
        cy.contains('Log in').click()
        cy.clearCookies()
        cy.get('#email').type('fab30@hotmail.com')
        cy.get('#password').type('littledog')
        cy.wait(200) 
        cy.get('.NewLoginForm_NewLogin_loginButton__2YO32 > .formButtons_button-primary__1Y09V').click()
        cy.title().should('contains', 'Zoro UK - Experts in Hand Tools, Power Tools and PPE')
        cy.contains('My Account')    
        
    })

    it('Zoro - failed login', ()=> {
        // the user visit "https://www.zoro.co.uk/" and lands on the home page
        // the user verify "Title" 
        // the user verify if the header contains "Log in" button
        // the user click on "Log in" button and "Sign in to your account" window pop-up
        // the user enters INVALID credentials
        // the user click on "Sign in button" and the test failed.
        // lastly the user verify the "Invalid email address or password" link

        cy.visit('https://www.zoro.co.uk/')
        cy.title().should('eq','Zoro UK - Experts in Hand Tools, Power Tools and PPE')
        cy.contains('Log in')
        cy.contains('Log in').click()
        cy.clearCookies()
        cy.get('#email').type('abcd@hotmail.com')
        cy.get('#password').type('abcd1234')
        cy.get('.NewLoginForm_NewLogin_loginButton__2YO32 > .formButtons_button-primary__1Y09V').click()
        cy.get('.NewLoginForm_NewLogin_loginError__5Ngwo').should('contain', 'Invalid email address or password')
        
    })

})